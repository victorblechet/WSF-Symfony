<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\Product;
use App\Form\CommentsType;
use App\Form\ProductType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route(path="/", name="homepage")
     */
    public function homepageAction(){
        $lastProducts = $this->getDoctrine()->getRepository(Product::class)->findFiveLast();
        return $this->render('index.html.twig',['products' => $lastProducts]);
    }


    /**
     * @Route(path="/products/", name="products")
     */
    public function indexAction(){
        $em = $this->getDoctrine();
        $products = $em->getRepository('App\Entity\Product')->findAll();
        $categories = $em->getRepository('App\Entity\Category')->findAll();
        return $this->render('productIndex.html.twig',['products'=> $products, "categories" => $categories]);
    }

    /**
     * @Route(path="/product/{id}", name="product")
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, Request $request){
        $em = $this->getDoctrine();
        /** @var Product $product */
        $product = $em->getRepository('App\Entity\Product')->find($id);
        $comments = $product->getComments();

        $comment = new Comments();
        $form = $this->createForm(CommentsType::class,$comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var Comments $comment */
            $comment = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $comment->setProduct($product);
            $em->persist($comment);
            $em->flush();
            return $this->redirect($request->getUri());
        }

        return $this->render(
            'productShow.html.twig',
            [
                'product' => $product,
                'commentForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(path="/product/create/", name="product_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction (Request $request){
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /** @var Product $product */
            $product = $form->getData();
            $manager = $this->getDoctrine()->getManager();

            $file = $product->getImage();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('kernel.root_dir') . '/../public/uploads',
                $fileName
            );

            $product->setImage($fileName);
            $manager->persist($product);
            $manager->flush();
            return $this->redirectToRoute('products');
        }

        return $this->render('addProduct.html.twig',['form' => $form->createView()]);
    }


}