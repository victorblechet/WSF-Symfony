<?php
/**
 * Created by PhpStorm.
 * User: Webschool37
 * Date: 27/09/2017
 * Time: 14:31
 */

namespace App\Controller;


use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route(path="/category/create/", name="category_new")
     * @param Request $request
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            $category = $form->getData();
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($category);
            $manager->flush();
            return $this->redirectToRoute('products');
        }
        return $this->render('addProduct.html.twig',['form' => $form->createView()]);
    }

    /**
     * @Route(path="/category/show/{id}", name="category_show")
     * @param Request $request
     * @param Category $category
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine();
        /** @var Category $category */
        $category = $em->getRepository("App\Entity\Category")->find($id);
        $products = $category->getProducts();

        return $this->render("categoryShow.html.twig",["category" => $category, "products" => $products]);
    }
}