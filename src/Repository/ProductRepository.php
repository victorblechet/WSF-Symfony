<?php
/**
 * Created by PhpStorm.
 * User: Webschool37
 * Date: 22/09/2017
 * Time: 16:30
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository extends EntityRepository
{
    public function findFiveLast(){
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App:Product p ORDER BY p.id DESC '
            )
            ->setMaxResults(5)
            ->getResult();
    }
}