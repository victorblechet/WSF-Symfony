<?php
/**
 * Created by PhpStorm.
 * User: Webschool37
 * Date: 27/09/2017
 * Time: 16:54
 */

namespace App\Form;


use App\Entity\Comments;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('content',CKEditorType::class)
            ->add('save', SubmitType::class, array('label' => 'Poster votre commentaire'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            ['data_class' => Comments::class,]
        );
    }
}