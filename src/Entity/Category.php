<?php
/**
 * Created by PhpStorm.
 * User: Webschool37
 * Date: 21/09/2017
 * Time: 16:10
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\OneToMany;
use App\Entity\Product;


/**
 * Class Category
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="category")
 * @ApiResource()
 */

class Category
{
    /**
     *@ORM\Column(type="integer")
     *@ORM\Id
     *@ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     * @var Collection
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    public function setProducts(Collection $products)
    {
        $this->products = $products;
    }

    public function __toString()
    {
        return $this->name;
    }


}