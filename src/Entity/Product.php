<?php
/**
 * Created by PhpStorm.
 * User: Webschool37
 * Date: 20/09/2017
 * Time: 08:59
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use App\Entity\Category;
/**
 * Class Product
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="product")
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */


class Product
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $description;


    /**
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Image()
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @var Category
     */
    private $category;

    /**
     *
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="product")
     * @var Collection
     * @ORM\OrderBy({"id" = "desc"})
     */
    private $comments;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @param Collection $comments
     */
    public function setComments(Collection $comments)
    {
        $this->comments = $comments;
    }



    public function __toString()
    {
        return $this->name;
    }


}