-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 01 oct. 2017 à 19:37
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `wsfsymfony`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(3, 'Les belles voyelles'),
(4, 'Les pas belles voyelles');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5F9E962A4584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `product_id`, `name`, `content`) VALUES
(6, 6, 'ConsonneFamily', '<p>LOOOOOOOOOL 0 flow</p>');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `price`, `description`, `image`) VALUES
(2, 3, 'A', 50, '<p><em>L<strong>&#39;</strong>essence</em> m&ecirc;me de la langue fran&ccedil;aise, &agrave; consommer <strong>sans mod&eacute;ration</strong></p>', 'cf09e9fccbf22e5287c6ef784202b78b.jpeg'),
(3, 3, 'E', 50, '<p>Le<strong> E&nbsp;</strong>a ce petit quelque chose qui le diff&eacute;rencie des autres voyelles. Dommage pour lui il n&#39;est pas pr&eacute;sent dans le pr&eacute;nom Victor ce qui fait diminuer son prix.&nbsp;</p>', '87215301b335364c608c80802fb3641b.jpeg'),
(4, 4, 'I', 10, '<p><strong>Simpliste</strong> et premi&egrave;re lettre de &quot;imb&eacute;cile&quot; le<strong> I&nbsp;</strong>est une lettre cr&eacute;&eacute;e pour que Jacquouille arrive tout de m&ecirc;me a &eacute;crire une lettre.&nbsp;</p>', 'e3dc66bbafb5a246d1b05289e96e2062.png'),
(5, 3, 'O', 100, '<p>L&#39;arrondie, la gr&acirc;ce, la simplicit&eacute;. Le O poss&egrave;de tellement d&#39;avantages que je vous laisse l&#39;admirer.&nbsp;</p>', '76ae3686c0e2708c7367d57ab3ba2d04.jpeg'),
(6, 4, 'U', 19, '<p>Le U est surnomm&eacute; &quot;t&ecirc;te de cuvette de chiotte&quot; par ses compatriotes les voyelles. C&#39;est pour dire !&nbsp;</p>', '1632efce21d589a963a753d7ec0f768f.jpeg'),
(7, 4, 'Y', 5, '<p>L&#39;existence de cette lettre reste encore un grand myst&egrave;re&nbsp;</p>', '07ab0d4120e2f4662bfcf346ab1a4ae4.jpeg');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_5F9E962A4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
